﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class ItemPanier
    {
        [Required]
        public ItemMenu Item { get; set; }
        public int Quantité { get; set; }

    }
}
