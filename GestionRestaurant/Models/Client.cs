﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Le nom est requis.")]
        [StringLength(100)]
        public string Nom { get; set; }

        [Required(ErrorMessage = "L'adresse est requise.")]
        [StringLength(200)]
        public string Adresse { get; set; }

        [Required(ErrorMessage = "Le numéro de téléphone est requis.")]
        [StringLength(30)]
        [Phone(ErrorMessage = "Le numéro de téléphone n'est pas valide.")]
        [Display(Name = "Numéro de téléphone")]
        public string Téléphone { get; set; }

    }
}
