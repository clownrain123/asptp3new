﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public enum ÉtatCommande
    {
        [Display(Name = "En préparation")]
        EnPréparation,
        [Display(Name = "En route")]
        EnLivraison,
        [Display(Name = "Complété")]
        Complété,
    }

    public class Commande
    {
        public Commande()
        {
            Items = new List<ItemCommande>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public ICollection<ItemCommande> Items { get; set; }

        public Guid? LivreurId { get; set; }
        public Livreur Livreur { get; set; }

        [Required]
        public Guid ClientId { get; set; }

        public Client Client { get; set; }
        
        public ÉtatCommande État { get; set; }
        
        [Display(Name = "Heure de la commande")]
        public DateTime MomentCommandé { get; set; }

        [Display(Name = "Livraison estimée")]
        public DateTime MomentLivraisonEstimé { get; set; }

        [Display(Name = "Heure de livraison")]
        public DateTime? MomentLivré { get; set; }

        [NotMapped]
        public double PrixTotal { get => Items.Sum(_ => _.PrixTotal); }

    }
}
