﻿using System;
using System.Collections.Generic;

namespace GestionRestaurant.Models
{
    public interface IPanier
    {
        ICollection<ItemPanier> Items { get; set; }

        void AjouterItem(ItemMenu item, int quantité);
        void AjusterQuantité(Guid id, int quantité);

        double Total { get; }

        void Vider();
    }
}