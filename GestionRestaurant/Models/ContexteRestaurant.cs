﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace GestionRestaurant.Models
{
    public class ContexteRestaurant
      : IdentityDbContext
    {
        const string CATEGORIE_PIZZAS = "Pizzas";
        const string CATEGORIE_DESSERTS = "Desserts";
        const string CATEGORIE_BREUVAGES = "Breuvages";
        const string CATEGORIE_AUTRE = "À Côtés";


        private static readonly Dictionary<Guid, string> IMAGES_SEED = new Dictionary<Guid, string>(new KeyValuePair<Guid, string>[]
        {
            new KeyValuePair<Guid, string>(new Guid("185b5f1d-80ec-458b-95cb-e00e901b3406"), "peperonni.png"),
            new KeyValuePair<Guid, string>(new Guid("a252df95-317e-4d82-8367-c95138934b37"), "légumes.png"),
            new KeyValuePair<Guid, string>(new Guid("72d54fd4-5cf4-461b-86d7-3fb8f23dfda5"), "hawaïenne.png"),
            new KeyValuePair<Guid, string>(new Guid("6c701dcf-6960-45d7-9a57-b54202a35b1d"), "toute garnie.png"),
            new KeyValuePair<Guid, string>(new Guid("9e027e34-9da7-4683-8362-38104603a6d8"), "biscuit.png"),
            new KeyValuePair<Guid, string>(new Guid("762d1473-6a87-4339-85c1-aced70dd82d0"), "brownies.png"),
            new KeyValuePair<Guid, string>(new Guid("a1346ec8-fd6c-47f1-942c-03cc4b2bcdab"), "7up.png"),
            new KeyValuePair<Guid, string>(new Guid("fb7f1dc9-862c-4eba-96d9-1f99dbe2c6d1"), "pepsi.png"),
        });

        public ContexteRestaurant() : base(){}
        public ContexteRestaurant(DbContextOptions options)
          : base(options)
        {
        }

        public DbSet<ItemMenu> Menu { get; set; }
        public DbSet<CatégorieMenu> Catégories { get; set; }
        public DbSet<Commande> Commandes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Livreur> Livreurs { get; set; }
        public DbSet<ApplicationUser> Utilisateurs { get; set; }

        public IQueryable<ItemMenu> MenuActif { get => Menu.Where(_ => _.Actif == true); }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseInMemoryDatabase("restaurant");
            }
        }

        protected async override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = "Admin",
                NormalizedName = "Administrateur".ToUpper()
            }, new IdentityRole
            {
                Name = "Livreur",
                NormalizedName = "Livreur".ToUpper()
            }) ;

            base.OnModelCreating(modelBuilder);

            Contract.Assert(modelBuilder != null);

            modelBuilder.Entity<ItemMenu>()
                .Property(_ => _.Actif).HasDefaultValue(true);

            modelBuilder.Entity<CatégorieMenu>()
                .HasData(ObtenirSeedCatégories());

            modelBuilder.Entity<ItemMenu>()
                .HasData(ObtenirSeedMenu());

            modelBuilder.Entity<ItemCommande>()
                .HasOne(_ => _.Item)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Commande>()
                .HasMany(_ => _.Items)
                .WithOne()
                .IsRequired();

            modelBuilder.Entity<Client>()
                .HasMany<Commande>()
                .WithOne(_ => _.Client)
                .OnDelete(DeleteBehavior.Restrict);

        }

        private IEnumerable<CatégorieMenu> ObtenirSeedCatégories()
        {
            return new CatégorieMenu[]
            {
                new CatégorieMenu(CATEGORIE_PIZZAS, 100),
                new CatégorieMenu(CATEGORIE_BREUVAGES, 200),
                new CatégorieMenu(CATEGORIE_DESSERTS, 300),
                new CatégorieMenu(CATEGORIE_AUTRE, 400),
            };
        }

        private IEnumerable<ItemMenu> ObtenirSeedMenu()
        {
            // Inspiration: Pizza Hut! Je reçois plein de pub sur mon Facebook maintenant...
            return new ItemMenu[]
            {
                new ItemMenu() {
                    Id = new Guid("185b5f1d-80ec-458b-95cb-e00e901b3406"),
                    Actif = true,
                    Catégorie = CATEGORIE_PIZZAS,
                    Nom = "Pizza au pépéronni",
                    Description = "Portion double de pepperoni et fromage mozzarella à profusion",
                    Prix = 15.99 },
                new ItemMenu() {
                    Id = new Guid("a252df95-317e-4d82-8367-c95138934b37"),
                    Actif = true,
                    Catégorie = CATEGORIE_PIZZAS,
                    Nom = "Pizza aux légumes",
                    Description = "Champignons, poivrons verts, oignons rouges, tomates mûres et fromage mozzarella.",
                    Prix = 16.99 },
                new ItemMenu() {
                    Id = new Guid("72d54fd4-5cf4-461b-86d7-3fb8f23dfda5"),
                    Actif = true,
                    Catégorie = CATEGORIE_PIZZAS,
                    Nom = "Pizza hawaïenne",
                    Description = "Jambon, ananas et généreuse portion de fromage mozzarella.",
                    Prix = 19.99 },
                new ItemMenu() {
                    Id = new Guid("6c701dcf-6960-45d7-9a57-b54202a35b1d"),
                    Actif = true,
                    Catégorie = CATEGORIE_PIZZAS,
                    Nom = "Pizza toute garnie",
                    Description = "Pepperoni, champignons, poivrons verts et fromage mozzarella.",
                    Prix = 17.49 },
                new ItemMenu() {
                    Id = new Guid("9e027e34-9da7-4683-8362-38104603a6d8"),
                    Actif = true,
                    Catégorie = CATEGORIE_DESSERTS,
                    Nom = "Biscuit",
                    Description = "Fraîchement cuit. Servi chaud et moelleux. Essayez-le aujourd’hui!",
                    Prix = 4.99 },
                new ItemMenu() {
                    Id = new Guid("762d1473-6a87-4339-85c1-aced70dd82d0"),
                    Actif = true,
                    Catégorie = CATEGORIE_DESSERTS,
                    Nom = "Brownies",
                    Description = "9 carrés moelleux sortis du four.",
                    Prix = 9.49 },
                new ItemMenu() {
                    Id = new Guid("a1346ec8-fd6c-47f1-942c-03cc4b2bcdab"),
                    Actif = true,
                    Catégorie = CATEGORIE_BREUVAGES,
                    Nom = "7-up (2L)",
                    Description = "Gâtez vos papilles avec le goût de lime et de citron frais de 7UP®. Sans caféine, mais des saveurs naturelles à 100 %. 7UP®, le goût de l’effervescence.",
                    Prix = 3.49 },
                new ItemMenu() {
                    Id = new Guid("fb7f1dc9-862c-4eba-96d9-1f99dbe2c6d1"),
                    Actif = true,
                    Catégorie = CATEGORIE_BREUVAGES,
                    Nom = "Pepsi (2L)",
                    Description = "La boisson gazeuse audacieuse, vivifiante et délicieusement pétillante.",
                    Prix = 3.49 },
            };
        }

        /**
         * Simplement parce que les images prennent trop de place dans la migration et 
         * ralentissent le temps de compilation considérablement, nous les ajoutons différemment.
         */
        internal void ValiderImagesSeed()
        {
            var guidsInitiaux = IMAGES_SEED.Keys.ToArray();
            var itemsInitiaux = Menu.Where(_ => guidsInitiaux.Contains(_.Id));

            // Ne change les photos que si les items initiaux ne semble pas avoir été modifiés.
            if (guidsInitiaux.Length == itemsInitiaux.Count() && itemsInitiaux.Count() == itemsInitiaux.Count(_ => _.Photo == null))
            {
                foreach (var item in itemsInitiaux)
                {
                    item.PhotoName = IMAGES_SEED[item.Id];
                    item.PhotoMimeType = "image/png";
                    item.Photo = this.ObtenirResource(IMAGES_SEED[item.Id]);
                }
                SaveChanges();
            }
        }

        private byte[] ObtenirResource(string nom)
        {
            using (var stream = new System.IO.FileStream(System.IO.Path.Join("Resources", nom), System.IO.FileMode.Open))
            {
                byte[] contenu = new byte[stream.Length];
                stream.Read(contenu, 0, contenu.Length);
                return contenu;
            }
        }

    }
}
