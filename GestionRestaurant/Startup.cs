using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Models;
using GestionRestaurant.MVC;
using GestionRestaurant.Session;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Stripe;

namespace GestionRestaurant
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ContexteRestaurant>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Restaurant"));
                //options.UseInMemoryDatabase("restaurant");
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddDefaultIdentity<ApplicationUser>()
                .AddRoles<IdentityRole>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ContexteRestaurant>();

            services.AddAuthorization(options =>
            { 
                options.AddPolicy("PageAdministrateur", policy => policy.RequireRole("Admin"));
                options.AddPolicy("PageLivreur", policy => policy.RequireRole("Livreur"));
            });

            services.AddHttpContextAccessor();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddPanier();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddRazorPagesOptions(options =>
            {
                options.Conventions.AuthorizeFolder("/Administration", "PageAdministrateur");
                options.Conventions.AuthorizeFolder("/Menu", "PageAdministrateur");
                options.Conventions.AuthorizeFolder("/Livreurs", "PageAdministrateur");
                options.Conventions.AuthorizeFolder("/Livreurs/Commandes", "PageAdministrateur");
                options.Conventions.AuthorizeFolder("/Livreurs/Commandes", "PageLivreur");
            });

            services.Configure<StripeSettings>(Configuration.GetSection("Stripe"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ContexteRestaurant db, UserManager<ApplicationUser> userManager)
        {
            Contract.Assert(db != null, "db");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseSession();

            StripeConfiguration.SetApiKey(Configuration.GetSection("Stripe")["SecretKey"]);

            if (db.Database.IsInMemory())
            {
                db.Database.EnsureCreated();
            }
            else
            {
                db.Database.Migrate();
                db.ValiderImagesSeed();
            }
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "default",
                  template: "{controller=Home}/{action=Index}/{id?}");
            });

            ApplicationDbInitializer.SeedUsers(userManager);

            ImagesController controller = new ImagesController(Configuration);
            controller.GetImages();
        }
    }
}
