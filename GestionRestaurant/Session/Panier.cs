﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace GestionRestaurant.Session
{
    public class Panier : Models.Panier
    {
        private const string ITEMS_SESSION_KEY = "PANIER_ITEMS";

        HttpContext HttpContext { get; set; }
        public Panier(IHttpContextAccessor httpContextAccessor)
        {
            Contract.Assert(httpContextAccessor != null);

            HttpContext = httpContextAccessor.HttpContext;
            Load();
        }

        public override void AjusterQuantité(Guid id, int quantité)
        {
            base.AjusterQuantité(id, quantité);
            Save();
        }

        public override void Vider()
        {
            base.Vider();
            Save();
        }

        private void Load()
        {
            var jsonItems = HttpContext.Session.GetString(ITEMS_SESSION_KEY);

            if (jsonItems != null)
            {
                Items = JsonConvert.DeserializeObject<ICollection<Models.ItemPanier>>(jsonItems);
            }
        }

        private void Save()
        {
            HttpContext.Session.SetString(ITEMS_SESSION_KEY, JsonConvert.SerializeObject(Items));
        }

    }

    public static class PanierExtensions
    {
        public static void AddPanier(this IServiceCollection services)
        {
            services.AddScoped<Models.IPanier, Panier>();
        }
    }
}
