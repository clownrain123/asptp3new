## TP1 - 420-174

Vous devrez créer une application web simple, en utilisant ASP.NET Core 2.2, qui permettra de gérer un restaurant de type pizzeria. Bien que la connexion et gestion des utilisateurs connectés ne fassent pas partie de cette version, vous aurez également à supporter différents types d’utilisateur, ou scénarios d’utilisations si vous préférez.
Comme vous êtes rendu à votre dernière année, je vous laisse plus de liberté sur les détails techniques comme la modélisation de la base de données et l’interface graphique. Si ce n’est pas spécifié, c’est votre choix et il doit avoir du sens!

## URL de déploiement
https://gestionrestaurant.azurewebsites.net/
