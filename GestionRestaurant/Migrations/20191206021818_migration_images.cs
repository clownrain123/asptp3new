﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GestionRestaurant.Migrations
{
    public partial class migration_images : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhotoName",
                table: "Menu",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "ab2194e7-76c4-4461-a33e-eb33d67faa72", "32898076-0bf2-4ffc-94b7-f896a04bae4b", "Admin", "ADMINISTRATEUR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "72c43141-8d73-4de8-8cec-d7cc5d02c4af", "458d9708-cdc6-454f-8a15-d38d4a30f67e", "Livreur", "LIVREUR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72c43141-8d73-4de8-8cec-d7cc5d02c4af");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ab2194e7-76c4-4461-a33e-eb33d67faa72");

            migrationBuilder.DropColumn(
                name: "PhotoName",
                table: "Menu");
        }
    }
}
