﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GestionRestaurant.Migrations
{
    public partial class creation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Catégories",
                columns: table => new
                {
                    Nom = table.Column<string>(maxLength: 50, nullable: false),
                    Ordre = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catégories", x => x.Nom);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Catégorie = table.Column<string>(maxLength: 50, nullable: false),
                    Nom = table.Column<string>(maxLength: 150, nullable: false),
                    Description = table.Column<string>(maxLength: 400, nullable: true),
                    Prix = table.Column<double>(nullable: false),
                    PhotoMimeType = table.Column<string>(nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    Actif = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Menu_Catégories_Catégorie",
                        column: x => x.Catégorie,
                        principalTable: "Catégories",
                        principalColumn: "Nom",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Catégories",
                columns: new[] { "Nom", "Ordre" },
                values: new object[,]
                {
                    { "Pizzas", 100 },
                    { "Breuvages", 200 },
                    { "Desserts", 300 },
                    { "À Côtés", 400 }
                });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "Id", "Actif", "Catégorie", "Description", "Nom", "Photo", "PhotoMimeType", "Prix" },
                values: new object[,]
                {
                    { new Guid("185b5f1d-80ec-458b-95cb-e00e901b3406"), true, "Pizzas", "Portion double de pepperoni et fromage mozzarella à profusion", "Pizza au pépéronni", null, null, 15.99 },
                    { new Guid("a252df95-317e-4d82-8367-c95138934b37"), true, "Pizzas", "Champignons, poivrons verts, oignons rouges, tomates mûres et fromage mozzarella.", "Pizza aux légumes", null, null, 16.989999999999998 },
                    { new Guid("72d54fd4-5cf4-461b-86d7-3fb8f23dfda5"), true, "Pizzas", "Jambon, ananas et généreuse portion de fromage mozzarella.", "Pizza hawaïenne", null, null, 19.989999999999998 },
                    { new Guid("6c701dcf-6960-45d7-9a57-b54202a35b1d"), true, "Pizzas", "Pepperoni, champignons, poivrons verts et fromage mozzarella.", "Pizza toute garnie", null, null, 17.489999999999998 },
                    { new Guid("a1346ec8-fd6c-47f1-942c-03cc4b2bcdab"), true, "Breuvages", "Gâtez vos papilles avec le goût de lime et de citron frais de 7UP®. Sans caféine, mais des saveurs naturelles à 100 %. 7UP®, le goût de l’effervescence.", "7-up (2L)", null, null, 3.4900000000000002 },
                    { new Guid("fb7f1dc9-862c-4eba-96d9-1f99dbe2c6d1"), true, "Breuvages", "La boisson gazeuse audacieuse, vivifiante et délicieusement pétillante.", "Pepsi (2L)", null, null, 3.4900000000000002 },
                    { new Guid("9e027e34-9da7-4683-8362-38104603a6d8"), true, "Desserts", "Fraîchement cuit. Servi chaud et moelleux. Essayez-le aujourd’hui!", "Biscuit", null, null, 4.9900000000000002 },
                    { new Guid("762d1473-6a87-4339-85c1-aced70dd82d0"), true, "Desserts", "9 carrés moelleux sortis du four.", "Brownies", null, null, 9.4900000000000002 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Menu_Catégorie",
                table: "Menu",
                column: "Catégorie");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Catégories");
        }
    }
}
