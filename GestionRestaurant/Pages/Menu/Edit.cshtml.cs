﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Http;

namespace GestionRestaurant.Pages.Menu
{
    public class EditModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public EditModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> Catégories { get; private set; }

        [BindProperty]
        public ItemMenu Item { get; set; }

        [BindProperty]
        public IFormFile Photo { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Item = await _context.Menu.FirstOrDefaultAsync(m => m.Id == id);

            if (Item == null)
            {
                return NotFound();
            }

            await LoadData();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Photo != null && !Photo.ContentType.StartsWith("image/", StringComparison.InvariantCulture))
            {
                ModelState.AddModelError(nameof(Photo), "La photo doit être une image valide.");
            }

            if (!ModelState.IsValid)
            {
                await LoadData();
                return Page();
            }

            var databaseItem = await _context.Menu.FindAsync(Item.Id);
            databaseItem.Nom = Item.Nom;
            databaseItem.Description = Item.Description;
            databaseItem.Prix = Item.Prix;
            databaseItem.Catégorie = Item.Catégorie;
            databaseItem.Actif = Item.Actif;

            if (Photo != null)
            {
                using (var stream = Photo.OpenReadStream())
                {
                    databaseItem.PhotoMimeType = Photo.ContentType;
                    databaseItem.Photo = new byte[Photo.Length];
                    await stream.ReadAsync(databaseItem.Photo, 0, databaseItem.Photo.Length);
                }
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemMenuExists(Item.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ItemMenuExists(Guid id)
        {
            return _context.Menu.Any(e => e.Id == id);
        }

        private async Task LoadData()
        {
            Catégories = await _context.Catégories.Select(_ => new SelectListItem(_.Nom, _.Nom)).ToListAsync();
        }
    }
}
