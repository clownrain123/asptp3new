﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Identity;

namespace GestionRestaurant.Pages.Livreurs
{
    public class CreateModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;
        private UserManager<ApplicationUser> Manager;

        public CreateModel(GestionRestaurant.Models.ContexteRestaurant context, UserManager<ApplicationUser> manager)
        {
            _context = context;
            Manager = manager;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Livreur Livreur { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            if (Manager.FindByNameAsync(Livreur.Nom).Result != null)
            {
                return Page();
            }

            ApplicationUser user = new ApplicationUser()
            {
                UserName = Livreur.Nom.ToLower() + "@pizzeriafeu.com",
                Email = Livreur.Nom.ToLower() + "@pizzeriafeu.com"
            };

            IdentityResult result = Manager.CreateAsync(user, "P@ssw0rd").Result;
            if (result.Succeeded)
            {
                await Manager.AddToRoleAsync(user, "LIVREUR");
            }

            _context.Livreurs.Add(Livreur);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}