﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Identity;

namespace GestionRestaurant.Pages.Livreurs
{
    public class DeleteModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;
        private UserManager<ApplicationUser> Manager;

        public DeleteModel(GestionRestaurant.Models.ContexteRestaurant context, UserManager<ApplicationUser> manager)
        {
            _context = context;
            Manager = manager;
        }

        [BindProperty]
        public Livreur Livreur { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Livreur = await _context.Livreurs
                .Include(_ => _.Commandes)
                .SingleOrDefaultAsync(_ => _.Id == id);

            if (Livreur == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Livreur = await _context.Livreurs
                .Include(_ => _.Commandes)
                .SingleOrDefaultAsync(_ => _.Id == id);

            if (Livreur != null)
            {
                if (Livreur.Commandes.Any())
                {
                    Livreur.Désactivé = true;
                }
                else
                {
                    ApplicationUser user = await Manager.FindByNameAsync(Livreur.Nom);
                    Manager.DeleteAsync(user).Wait();
                    _context.Livreurs.Remove(Livreur);
                }

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
