﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Livreurs
{
    public class IndexModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public IndexModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public IList<Livreur> Livreurs { get;set; }

        public async Task OnGetAsync()
        {
            Livreurs = await _context.Livreurs.Include(_ => _.Commandes).ToListAsync();
        }
    }
}
