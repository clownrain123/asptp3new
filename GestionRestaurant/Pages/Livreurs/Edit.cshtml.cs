﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Livreurs
{
    public class EditModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public EditModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        [BindProperty]
        public Livreur Livreur { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Livreur = await _context.Livreurs.FirstOrDefaultAsync(m => m.Id == id);

            if (Livreur == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Livreur).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LivreurExists(Livreur.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool LivreurExists(Guid id)
        {
            return _context.Livreurs.Any(e => e.Id == id);
        }
    }
}
