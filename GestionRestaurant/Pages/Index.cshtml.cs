﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace GestionRestaurant.Pages
{
    public class IndexModel : PageModel
    {
        private ContexteRestaurant Restaurant { get; }
        public IndexModel(ContexteRestaurant restaurant)
        {
            Restaurant = restaurant;
        }

        public IEnumerable<CatégorieMenu> Catégories { get; private set; }

        public async Task OnGetAsync()
        {
            Catégories = await (from cat in Restaurant.Catégories
                                where cat.Items.Any(item => item.EstActif)
                                orderby cat.Ordre
                                select cat)
                                .Include(_ => _.Items)
                                .ToListAsync();
        }

    }
}
