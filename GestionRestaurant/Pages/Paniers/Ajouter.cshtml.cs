﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GestionRestaurant.Pages.Paniers
{
    public class AjouterModel : PageModel
    {
        private Models.ContexteRestaurant Restaurant { get; set; }
        private Models.IPanier Panier { get; set; }

        public AjouterModel(Models.ContexteRestaurant restaurant,  Models.IPanier panier)
        {
            Restaurant = restaurant;
            Panier = panier;
        }


        [BindProperty(Name = "Item")]
        public Guid ItemId { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            var item = await Restaurant.Menu.FindAsync(ItemId);
            if (item == null)
            {
                return BadRequest();
            }

            Panier.AjouterItem(item, 1);                
            return Redirect("~/Paniers");
        }

    }
}