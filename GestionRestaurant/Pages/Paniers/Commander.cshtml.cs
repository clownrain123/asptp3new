﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GestionRestaurant.Models;
using GestionRestaurant.Session;

namespace GestionRestaurant.Pages.Paniers
{
    public class CommanderModel : PageModel
    {
        private Models.ContexteRestaurant Restaurant { get; set; }
        private Models.IPanier Panier { get; set; }


        public CommanderModel(GestionRestaurant.Models.ContexteRestaurant restaurant, Models.IPanier panier)
        {
            Restaurant = restaurant;
            Panier = panier;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await AssignerCommande();

            return Page();
        }

        public Commande Commande { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await AssignerCommande();

            foreach (var item in Commande.Items)
            {

            }

            Restaurant.Commandes.Add(Commande);
            await Restaurant.SaveChangesAsync();

            Panier.Vider();

            return RedirectToPage("/Commandes/Details", new { Id = Commande.Id });
        }

        private async Task AssignerCommande()
        {
            Commande = new Commande();
            Commande.État = ÉtatCommande.EnPréparation;
            Commande.MomentCommandé = DateTime.Now;
            Commande.MomentLivraisonEstimé = DateTime.Now.AddMinutes(30);
            Commande.Client = await Restaurant.Clients.FindAsync(HttpContext.Session.GetIdClient());
            Commande.Items = Panier.Items.Select(_ => new ItemCommande(_)).ToList();
            foreach (var item in Commande.Items)
            {
                item.Item = await Restaurant.Menu.FindAsync(item.Item.Id);
            }

        }
    }
}