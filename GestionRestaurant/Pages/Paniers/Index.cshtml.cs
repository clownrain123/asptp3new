using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Session;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Stripe;

namespace GestionRestaurant.Pages.Paniers

{
    public class IndexModel : PageModel
    {
        private Models.ContexteRestaurant Restaurant { get; set; }
        public Models.IPanier Panier { get; set; }

        public IndexModel(Models.ContexteRestaurant restaurant, Models.IPanier panier)
        {
            Restaurant = restaurant;
            Panier = panier;
        }

        [BindProperty]
        public Dictionary<string, int> Items { get; set; }


        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            AjusterPanier();
            return Page();
        }

        public async Task<IActionResult> OnPostSupprimer(Guid id)
        {
            var item = await Restaurant.Menu.FindAsync(id);
            if (item == null)
            {
                return BadRequest();
            }

            AjusterPanier(); // Ajuste le panier avec le contenu du post, puis supprime l'élément demandé
            Panier.AjusterQuantité(item.Id, 0);
            return Page();
        }

        public IActionResult OnPostCommander()
        {
            AjusterPanier();

            if (HttpContext.Session.GetIdClient() == null)
            {
                return Redirect("~/Clients/Creer");
            }
            else
            {
                return Redirect("~/Paniers/Commander");
            }
        }
        public IActionResult Charge(string stripeToken)
        {

            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys

            StripeConfiguration.ApiKey = "sk_test_kaFI3ObL3yYABrYN12dlnACT00JgXIxenk";

            // Token is created using Checkout or Elements!
            // Get the payment token submitted by the form:
            //var token = model.Token; // Using ASP.NET MVC

            var options = new ChargeCreateOptions
            {
                Amount = 999,
                Currency = "cad",
                Description = "Exemple charge",
                Source = stripeToken,
            };
            var service = new ChargeService();
            Charge charge = service.Create(options);

            if (charge.Paid)
            {
                return Redirect("~/Paniers/Commander");
            }
            //Request.Form
            return Page();
        }
        protected void AjusterPanier()
        {
            foreach (var item in Items)
            {
                if (Guid.TryParse(item.Key, out var guid))
                {
                    Panier.AjusterQuantité(guid, item.Value);
                }
            }
        }
    }
}
