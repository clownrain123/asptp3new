﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Commandes
{
    public class EditModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public EditModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        [BindProperty]
        public Commande Commande { get; set; }

        public IEnumerable<SelectListItem> Livreurs { get; private set; }

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            await LoadPageInfo(id);

            if (Commande == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            try
            {
                var commande = await _context.Commandes.FindAsync(Commande.Id);
                if (Commande.LivreurId != commande.LivreurId)
                {
                    commande.LivreurId = Commande.LivreurId;
                    commande.État = ÉtatCommande.EnLivraison;
                    await _context.SaveChangesAsync();
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommandeExists(Commande.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("../Administration/Index");
        }

        private bool CommandeExists(Guid id)
        {
            return _context.Commandes.Any(e => e.Id == id);
        }

        private async Task LoadPageInfo(Guid id)
        {
            Commande = await _context.Commandes
                .Include(_ => _.Items)
                .ThenInclude(_ => _.Item)
                .FirstOrDefaultAsync(m => m.Id == id);

            Livreurs = await(from livreur in _context.Livreurs
                             where livreur.Désactivé == false
                             select new SelectListItem(livreur.Nom, livreur.Id.ToString()))
                              .ToListAsync();

        }
    }
}
