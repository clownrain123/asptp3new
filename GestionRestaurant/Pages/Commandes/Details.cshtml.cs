﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Commandes
{
    public class DetailsModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public DetailsModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public Commande Commande { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Commande = await _context.Commandes
                .Include(_ => _.Items)
                .ThenInclude(_ => _.Item)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (Commande == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
