﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Administration
{
    public class StatistiqueLivreur
    {
        public Livreur Livreur { get; set; }
        public int QuantitéEnRetard { get; set; }
        public int QuantitéÀTemps { get; set; }
        public int QuantitéEnCours { get; set; }
    }

    public class IndexModel : PageModel
    {

        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public IndexModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public IEnumerable<Commande> Commandes { get; private set; }
        public IEnumerable<StatistiqueLivreur> Livreurs { get; private set; }

        public int NombreLivraisons { get; private set; }
        public double RecettesQuotidiennes { get; private set; }

        public async Task OnGetAsync()
        {
            var débutDeJournée = DateTime.Now.Date.AddDays(-1);
            var commandesQuotidiennes = _context.Commandes.Where(_ => _.MomentCommandé >= débutDeJournée);

            NombreLivraisons = await (from commande in commandesQuotidiennes
                                      select 1).CountAsync();

            RecettesQuotidiennes = await (from commande in _context.Commandes
                                          from item in commande.Items
                                          select item.PrixTotal).SumAsync();

            Commandes = await commandesQuotidiennes.OrderBy(_ => _.État)
                              .Include(_ => _.Livreur)
                              .Take(10)
                              .ToListAsync();

            Livreurs = await (from commande in commandesQuotidiennes
                              where commande.LivreurId != null
                              group commande by commande.Livreur into grp
                              select new StatistiqueLivreur()
                              {
                                  Livreur = grp.Key,
                                  QuantitéEnRetard = grp.Where(grpItem => grpItem.État == ÉtatCommande.Complété && grpItem.MomentLivré >= grpItem.MomentCommandé.AddMinutes(30)).Count(),
                                  QuantitéÀTemps = grp.Where(grpItem => grpItem.État == ÉtatCommande.Complété && grpItem.MomentLivré < grpItem.MomentCommandé.AddMinutes(30)).Count(),
                                  QuantitéEnCours = grp.Where(grpItem => grpItem.État != ÉtatCommande.Complété).Count()
                              })
                              .ToListAsync();
        }
    }
}
