﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace GestionRestaurant.MVC
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        IConfiguration config;
        CloudStorageAccount compte;
        CloudBlobClient client;
        CloudBlobContainer container;

        public ImagesController(IConfiguration config)
        {
            this.config = config;
            compte = CloudStorageAccount.Parse(config.GetConnectionString("BlobStorage"));
            client = compte.CreateCloudBlobClient();
            container = client.GetContainerReference("images");
            container.CreateIfNotExistsAsync();
            container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
        }

        public async void GetImages()
        {
            BlobResultSegment results = await container.ListBlobsSegmentedAsync(null);
            foreach (CloudBlockBlob image in results.Results)
            {
                if (!Directory.EnumerateFileSystemEntries("wwwroot/images").Contains(image.Name))
                {
                    using (var filestream = System.IO.File.OpenWrite("wwwroot/images/" + image.Name))
                    {
                        await image.DownloadToStreamAsync(filestream);
                    }
                }
            }
        }

        public async void UploadImage(IFormFile image)
        {
            if (image == null)
            {
                throw new ArgumentException();
            }

            CloudBlockBlob block = container.GetBlockBlobReference(image.FileName);
            using (var stream = new MemoryStream())
            {
                await image.CopyToAsync(stream);

                await block.UploadFromByteArrayAsync(stream.ToArray(), 0, stream.ToArray().Length - 1);
            }

            GetImages();
        }

        public async void DeleteImage(string nom)
        {

            CloudBlockBlob block = container.GetBlockBlobReference(nom);
            await block.DeleteAsync();

            GetImages();
        }
    }
}