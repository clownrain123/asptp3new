using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.MVC
{
    [Route("api/[controller]")]
    [ApiController]
    public class LivreursController : ControllerBase
    {
        private readonly ContexteRestaurant _context;

        public LivreursController(ContexteRestaurant context)
        {
            _context = context;
        }

        // PUT: api/Livreurs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> TerminerCommande(Guid id)
        {
            var commande = await _context.Commandes.FindAsync(id);
            if (commande == null)
            {
                return NotFound();
            }
            commande.État = ÉtatCommande.Complété;
            commande.MomentLivré = DateTime.Now;
            _context.Entry(commande).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LivreurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        [HttpPut("heure/{id}")]
        public async Task<IActionResult> PutHeureEstimee(Guid id, DateTime heureEstimee)
        {
            var commande = await _context.Commandes.FindAsync(id);
            if (commande == null)
            {
                return NotFound();
            }
            commande.MomentLivraisonEstimé = heureEstimee;
            _context.Entry(commande).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LivreurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // GET: api/Livreurs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Commande>>> GetLivraisons(Guid id)
        {
            var livreur = await _context.Livreurs.FindAsync(id);
            if (livreur == null)
            {
                return NotFound();
            }

            IEnumerable<Commande> commandes = _context.Commandes.Where(x => x.LivreurId == livreur.Id);
            var commandesLivreur = new JsonResult(commandes);
            commandesLivreur.ContentType = "application/json";
            return commandesLivreur;
        }

        private bool LivreurExists(Guid id)
        {
            return _context.Livreurs.Any(e => e.Id == id);
        }
    }
}
